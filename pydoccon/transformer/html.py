from lark import Token, Transformer

inline_types = {
    "bold_delimiter": "strong",
    "code_delimiter": "code",
    "italic_delimiter": "em",
}

class HtmlRenderer(Transformer):
    def start(self, children):
        return '\n'.join(children)

    def text(self, children):
        return ' '.join(children)

    def link(self, children):
        text, url = children

        return f'<a href="{url}">{text}</a>'

    def image_link(self, children):
        text, url = children

        return f'<img src="{url}" alt="{text}" />'

    def inline_delimiter(self, children):
        return children[0].data

    def inline(self, tree):
        """ Depending on number of children, handle inline markup """
        tree_size = len(tree)
        
        if tree_size == 2:
            if isinstance(tree[0], Token):
                element = inline_types[tree[1]]
                return f"{tree[0]}</{element}>"
            else:
                element = inline_types[tree[0]]
                return f"<{element}>{tree[1]}"
        elif tree_size == 3:
            element = inline_types[tree[0]]
            return f"<{element}>{tree[1]}</{element}>"
        else:
            return tree[0]

    def line(self, children):
        return ' '.join(children)

    def HEADER_TYPE(self, t):
        return len(t)

    def header(self, children):
        level, text = children[:2]

        return f"<h{level}>{text}</h{level}>"

    def block_quote(self, children):
        return f'<blockquote>{children[0]}</blockquote>'

    def code_block(self, children):
        if len(children) > 3:
            _class = f' class="language-{children[0]}"'
            content = children[2]
        else:
            _class = ''
            content = children[1]

        return f'<pre><code{_class}>{content}</code></pre>'

    def pre(self, children):
        return ''.join(children)

    def paragraph(self, children):
        child_string = ' '.join(children[:-1])

        return f"<p>{child_string}</p>"

    def ordered_list_item(self, children):
        return f"<li>{children[1]}</li>"

    def ordered_list(self, children):
        child_string = '\n'.join(children)

        return f"<ol>{child_string}</ol>"

    def unordered_list_item(self, children):
        return f"<li>{children[0]}</li>"

    def unordered_list(self, children):
        child_string = '\n'.join(children)

        return f"<ul>{child_string}</ul>"

    def horizontal_rule(self, _):
        return "<hr />"
