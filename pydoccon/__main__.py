from importlib import resources
import sys

from lark import Lark
from lark.exceptions import UnexpectedCharacters

from . import grammar
from .transformer.html import HtmlRenderer


if __name__ == '__main__':
    markdown_grammar = resources.read_text(grammar, 'markdown.lark')

    json_parser = Lark(markdown_grammar, parser='earley', lexer='dynamic')

    """
    json_parser = Lark(markdown_grammar,
        parser='lalr', lexer='standard',
        propagate_positions=False,
        maybe_placeholders=False)
    """

    try:
        with open(sys.argv[2]) as f:
            input_text = f.read()
    except IndexError:
        input_text = sys.stdin.read()
    except FileNotFoundError:
        sys.exit("File not FOUND")

    # print(input_text, file=sys.stderr, end='')

    if input_text[-1] != '\n':
        input_text += "\n\n"

    try:
        output_tree = json_parser.parse(input_text)
    except UnexpectedCharacters:
        sys.exit("We can't parse that!")

    mode = sys.argv[1]

    if mode == 'tree':
        print(output_tree.pretty())
    elif mode == 'convert':
        print(HtmlRenderer().transform(output_tree))
    else:
        print(f'Invalid mode "{mode}" specified!')
