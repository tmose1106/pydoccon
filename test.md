# Header

This is a paragraph

## Another Header

This paragraph contains **bold** and _italic text_ and `code text` too


This paragraph needs some **SPACE**, please



> This block gets its own space

This line contains [some hypertext](http://example.com) too

And image goes here ![my image](http://example.com) wow look at that

* This is a list item
* Here is another item
* And one more item for good luck

---

1. Order
2. Does In Fact
3. Matter

```python
# code block
print '3 backticks or'
print 'indent 4 spaces'
```

```
```
